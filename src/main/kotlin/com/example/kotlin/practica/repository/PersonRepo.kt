package com.example.kotlin.practica.repository

import com.example.kotlin.practica.entities.Person
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface PersonRepo: JpaRepository<Person,Int> {
    fun findByName(name: String): Person?
}