package com.example.kotlin.practica

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.ComponentScan

@SpringBootApplication
@ComponentScan("com.example")
class KotlinPracticaApplication

fun main(args: Array<String>) {
	runApplication<KotlinPracticaApplication>(*args)
}
