package com.example.kotlin.practica.controller

import com.example.kotlin.practica.entities.Person
import com.example.kotlin.practica.repository.PersonRepo
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RestController

@RestController
class PersonController(val personRepo: PersonRepo){

    @GetMapping("/persons")
    fun findAll(): List<Person>{
        return personRepo.findAll()
    }

    @GetMapping("/persons/{name}")
    fun getPersonByName(@PathVariable("name") name: String) = personRepo.findByName(name)
}